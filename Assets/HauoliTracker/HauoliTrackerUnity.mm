//
//  HauoliTrackerUnity.m
//  trackhand
//
//  Created by Lin Guan on 2/8/19.
//  Copyright © 2019 Hauoli. All rights reserved.
//

#import "HauoliTrackerUnity.h"

@interface HauoliTrackerUnity()
@end

@implementation HauoliTrackerUnity

  HauoliTrackerUnity *_sharedInstance;

+(HauoliTrackerUnity*) sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSLog(@"Creating HauoliTrackerUnity shared instance.");
        _sharedInstance = [[HauoliTrackerUnity alloc] init];
    });
    return _sharedInstance;
}
-(id)init
{
    self = [super init];
    hauoliTracker = [[HauoliTracker alloc] init];
    return self;
}
- (int)initTracker:(int)num_spk
           num_mic:(int)num_mic
          init_pos:(double *)init_pos
           spk_pos:(double *)spk_pos
           mic_pos:(double *)mic_pos
{
    return [hauoliTracker initTracker:num_spk num_mic:num_mic init_pos:init_pos spk_pos:spk_pos mic_pos:mic_pos];
}
- (int)start
{
    return [hauoliTracker start];
}
- (int)stop
{
    return [hauoliTracker stop];
}
- (double)getDist
{
    return [hauoliTracker getDist];
}
- (double *)getDistsHist:(int)spk_idx mic_idx:(int)mic_idx
{
    return [hauoliTracker getDistsHist:spk_idx mic_idx:mic_idx];
}
- (void)getDistsHist2:(double *)dists spk_idx:(int)spk_idx mic_idx:(int)mic_idx
{
    return [hauoliTracker getDistsHist2:dists spk_idx:spk_idx mic_idx:mic_idx];
}
- (void)getDists:(double *)dists
{
    [hauoliTracker getDists:dists];
}
- (void)getPos:(double *)pos
{
    [hauoliTracker getPos:pos];
}
- (double)getPosX
{
    return [hauoliTracker getPosX];
}
- (double)getPosY
{
    return [hauoliTracker getPosX];
}
- (double)getPosZ
{
    return [hauoliTracker getPosX];
}
- (int)getState
{
    return [hauoliTracker getState];
}
- (double)getPower
{
    return [hauoliTracker getPower];
}
- (double)getB
{
    return [hauoliTracker getB];
}
- (void)getFc:(int *)f
{
    [hauoliTracker getFc:f];
}
- (bool)getInternalData:(double *)data len:(int)len
{
    return [hauoliTracker getInternalData:data len:len];
}
- (void)getPlayBuffer:(short int *)buf spk_idx:(int)spk_idx
{
    [hauoliTracker getPlayBuffer:buf spk_idx:spk_idx];
}
- (bool)setDistEstMethod:(int)method
{
    return [hauoliTracker setDistEstMethod:method];
}
- (bool)setB:(double)b
{
    return [hauoliTracker setB:b];
}
- (bool)setFc:(int *)f
{
    return [hauoliTracker setFc:f];
}
- (bool)setSkipTime:(double)time
{
    return [hauoliTracker setSkipTime:time];
}
- (bool)setUseFile:(bool)if_file
{
    return [hauoliTracker setUseFile:if_file];
}
- (bool)setRecordAudio:(bool)if_record
{
    return [hauoliTracker setRecordAudio:if_record];
}
- (bool)setSaveDist:(bool)if_save
{
    return [hauoliTracker setSaveDist:if_save];
}
- (bool)setAudioSource:(int)src
{
    return [hauoliTracker setAudioSource:src];
}
- (bool)setResetMethod:(int)method
{
    return [hauoliTracker setResetMethod:method];
}
- (bool)pushAudio:(int8_t *)y num_elem:(int)num_elem
{
    return [hauoliTracker pushAudio:y num_elem:num_elem];
}
- (bool)pushAudio2:(int16_t *)y num_elem:(int)num_elem
{
    return [hauoliTracker pushAudio2:y num_elem:num_elem];
}
// Tap
- (int)getNSeq
{
    return [hauoliTracker getNSeq];
}
- (int)getNSeqUp
{
    return [hauoliTracker getNSeq];
}
- (int)getMinTap
{
    return [hauoliTracker getMinTap];
}
- (int)getMaxTap
{
    return [hauoliTracker getMaxTap];
}
- (int)getTapStep
{
    return [hauoliTracker getTapStep];
}
- (void)getCirDiff:(double *)cir_dif_real cir_dif_imag:(double *)cir_dif_imag
{
    return [hauoliTracker getCirDiff:cir_dif_real cir_dif_imag:cir_dif_imag];
}
- (bool)setSeq:(int)num_seq num_seq_up:(int)num_seq_up
{
    return [hauoliTracker setSeq:num_seq num_seq_up:num_seq_up];
}
// - (bool)setNSeq:(int)num_seq
// {
//     return [hauoliTracker setNSeq:num_seq];
// }
// - (bool)setNSeqUp:(int)num_seq_up
// {
//     return [hauoliTracker setNSeqUp:num_seq_up];
// }
- (bool)setMinTap:(int)min_tap
{
    return [hauoliTracker setMinTap:min_tap];
}
- (bool)setMaxTap:(int)max_tap
{
    return [hauoliTracker setMaxTap:max_tap];
}
- (bool)setTapStep:(int)tap_step
{
    return [hauoliTracker setTapStep:tap_step];
}
- (bool)setAutoThrd:(int)type thrd:(double)thrd
{
    return [hauoliTracker setAutoThrd:type thrd:thrd];
}
- (bool)setAutoThrd2:(int)type thrds:(double *)thrds
{
    return [hauoliTracker setAutoThrd2:type thrds:thrds];
}
- (bool)needCalibration
{
    return [hauoliTracker needCalibration];
}
- (bool)setAbsDistMagThrd:(double)thrd
{
    return [hauoliTracker setAbsDistMagThrd:thrd];
}
- (bool)setAbsDistDifThrd:(double)thrd
{
    return [hauoliTracker setAbsDistDifThrd:thrd];
}
- (bool)setAbsDistItvThrd:(double)thrd
{
    return [hauoliTracker setAbsDistItvThrd:thrd];
}
- (bool)setTapSelection:(double)method
{
    return [hauoliTracker setTapSelection:method];
}
- (bool)enableDoppler:(bool)enabled
{
    return [hauoliTracker enableDoppler:enabled];
}
- (double)getVel:(int)spk_idx mic_idx:(int)mic_idx
{
    return [hauoliTracker getVel:spk_idx mic_idx:mic_idx];
}
- (void)getCaliThrd:(double *)thrds
{
    return [hauoliTracker getCaliThrd:thrds];
}
// FMCW-sepcific
- (double)getChirpLenS
{
    return [hauoliTracker getChirpLenS];
}
- (bool)setChirpLenS:(double)chirp_len_s seq_len_s:(double)seq_len_s
{
    return [hauoliTracker setChirpLenS:chirp_len_s seq_len_s:seq_len_s];
}
- (bool)setCancelLenS:(double)len
{
    return [hauoliTracker setCancelLenS:len];
}
- (bool)setAGC:(bool)on
{
    return [hauoliTracker setAGC:on];
}
- (bool)setPlayAudio:(bool)play
{
    return [hauoliTracker setPlayAudio:play];
}
- (bool)setIfProcessAudio:(bool)process
{
    return [hauoliTracker setIfProcessAudio:process];
}
// Gesture
- (int)getGesture
{
    return [hauoliTracker getGesture];
}
- (int)getGestCnt
{
    return [hauoliTracker getGestCnt];
}
- (int)getGestDirChange
{
    return [hauoliTracker getGestDirChange];
}
- (bool)enableGesture:(bool)enabled
{
    return [hauoliTracker enableGesture:enabled];
}
// UI Related
- (bool)trajCleaned
{
    return [hauoliTracker trajCleaned];
}
// AAudio
- (void)setRecordingDeviceId:(int)id
{
    [hauoliTracker setRecordingDeviceId:id];
}
- (void)setPlaybackDeviceId:(int)id
{
    [hauoliTracker setPlaybackDeviceId:id];
}

@end


extern "C"
{
    int hauoli_initTracker(int num_spk, int num_mic, double* init_pos, double* spk_pos, double* mic_pos){
        return [[HauoliTrackerUnity sharedInstance] initTracker:num_spk num_mic:num_mic init_pos:init_pos spk_pos:spk_pos mic_pos:mic_pos];
    }

    int hauoli_start() {
        return [[HauoliTrackerUnity sharedInstance] start];
    }
    int hauoli_stop() {
        return [[HauoliTrackerUnity sharedInstance] stop];
    }
    
    double hauoli_getDist() {
        return [[HauoliTrackerUnity sharedInstance] getDist];
    }

    double *hauoli_getDistsHist(int spk_idx, int mic_idx) {
        return [[HauoliTrackerUnity sharedInstance] getDistsHist:spk_idx mic_idx:mic_idx];
    }

    void hauoli_getDistsHist2(double *dists, int spk_idx, int mic_idx) {
        return [[HauoliTrackerUnity sharedInstance] getDistsHist2:dists spk_idx:spk_idx mic_idx:mic_idx];
    }
      
    void hauoli_getDists(double* dists) {
        [[HauoliTrackerUnity sharedInstance] getDists:dists];
    }
      
    void hauoli_getPos(double* pos) {
        [[HauoliTrackerUnity sharedInstance] getPos:pos];
    }

    double hauoli_getPosX() {
        return [[HauoliTrackerUnity sharedInstance] getPosX];
    }

    double hauoli_getPosY() {
        return [[HauoliTrackerUnity sharedInstance] getPosY];
    }

    double hauoli_getPosZ() {
        return [[HauoliTrackerUnity sharedInstance] getPosZ];
    }
      
    int hauoli_getState() {
        return [[HauoliTrackerUnity sharedInstance] getState];
    }

    double hauoli_getPower() {
        return [[HauoliTrackerUnity sharedInstance] getPower];
    }
      
    double hauoli_getB() {
        return [[HauoliTrackerUnity sharedInstance] getB];
    }
    
    void hauoli_getFc(int *f) {
        [[HauoliTrackerUnity sharedInstance] getFc:f];
    }

    bool hauoli_getInternalData(double *data, int len) {
        return [[HauoliTrackerUnity sharedInstance] getInternalData:data len:len];
    }

    void hauoli_getPlayBuffer(short int *buf, int spk_idx) {
        [[HauoliTrackerUnity sharedInstance] getPlayBuffer:buf spk_idx:spk_idx];
    }
    
    bool hauoli_setDistEstMethod(int method) {
        return [[HauoliTrackerUnity sharedInstance] setDistEstMethod:method];
    }

    bool hauoli_setB(double b) {
        return [[HauoliTrackerUnity sharedInstance] setB:b];
    }
      
    bool hauoli_setFc(int *f) {
        return [[HauoliTrackerUnity sharedInstance] setFc:f];
    }
      
    bool hauoli_setSkipTime(double time) {
        return [[HauoliTrackerUnity sharedInstance] setSkipTime:time];
    }
      
    bool hauoli_setUseFile(bool if_file) {
        return [[HauoliTrackerUnity sharedInstance] setUseFile:if_file];
    }
      
    bool hauoli_setRecordAudio(bool if_record) {
        return [[HauoliTrackerUnity sharedInstance] setRecordAudio:if_record];
    }
      
    bool hauoli_setSaveDist(bool if_save) {
        return [[HauoliTrackerUnity sharedInstance] setSaveDist:if_save];
    }

    bool hauoli_setAudioSource(int src) {
        return [[HauoliTrackerUnity sharedInstance] setAudioSource:src];
    }
      
    bool hauoli_setResetMethod(int method) {
        return [[HauoliTrackerUnity sharedInstance] setResetMethod:method];
    }

    bool hauoli_pushAudio(int8_t *y, int num_elem) {
        return [[HauoliTrackerUnity sharedInstance] pushAudio:y num_elem:num_elem];
    }

    bool hauoli_pushAudio2(int16_t *y, int num_elem) {
        return [[HauoliTrackerUnity sharedInstance] pushAudio2:y num_elem:num_elem];
    }

    // Tap
    int hauoli_getNSeq() {
        return [[HauoliTrackerUnity sharedInstance] getNSeq];
    }
      
    int hauoli_getNSeqUp() {
        return [[HauoliTrackerUnity sharedInstance] getNSeqUp];
    }
      
    int hauoli_getMinTap() {
        return [[HauoliTrackerUnity sharedInstance] getMinTap];
    }
      
    int hauoli_getMaxTap() {
        return [[HauoliTrackerUnity sharedInstance] getMaxTap];
    }
      
    int hauoli_getTapStep() {
        return [[HauoliTrackerUnity sharedInstance] getTapStep];
    }

    void getCirDiff(double *cir_dif_real, double *cir_dif_imag) {
        [[HauoliTrackerUnity sharedInstance] getCirDiff:cir_dif_real cir_dif_imag:cir_dif_imag];
    }

    bool hauoli_setSeq(int num_seq, int num_seq_up) {
        return [[HauoliTrackerUnity sharedInstance] setSeq:num_seq num_seq_up:num_seq_up];
    }
      
    // bool hauoli_setNSeq(int num_seq) {
    //     return [[HauoliTrackerUnity sharedInstance] setNSeq:num_seq];
    // }
      
    // bool hauoli_setNSeqUp(int num_seq_up) {
    //     return [[HauoliTrackerUnity sharedInstance] setNSeqUp:num_seq_up];
    // }
      
    bool hauoli_setMinTap(int min_tap) {
        return [[HauoliTrackerUnity sharedInstance] setMinTap:min_tap];
    }
      
    bool hauoli_setMaxTap(int max_tap) {
        return [[HauoliTrackerUnity sharedInstance] setMaxTap:max_tap];
    }
      
    bool hauoli_setTapStep(int tap_step) {
        return [[HauoliTrackerUnity sharedInstance] setTapStep:tap_step];
    }

    bool hauoli_setAutoThrd(int type, double thrd) {
        return [[HauoliTrackerUnity sharedInstance] setAutoThrd:type thrd:thrd];
    }

    bool hauoli_setAutoThrd2(int type, double* thrds) {
        return [[HauoliTrackerUnity sharedInstance] setAutoThrd2:type thrds:thrds];
    }

    bool hauoli_needCalibration() {
        return [[HauoliTrackerUnity sharedInstance] needCalibration];
    }

    bool hauoli_setAbsDistMagThrd(double thrd) {
        return [[HauoliTrackerUnity sharedInstance] setAbsDistMagThrd:thrd];
    }

    bool hauoli_setAbsDistDifThrd(double thrd) {
        return [[HauoliTrackerUnity sharedInstance] setAbsDistDifThrd:thrd];
    }

    bool hauoli_setAbsDistItvThrd(double thrd) {
        return [[HauoliTrackerUnity sharedInstance] setAbsDistItvThrd:thrd];
    }

    bool hauoli_setTapSelection(int method) {
        return [[HauoliTrackerUnity sharedInstance] setTapSelection:method];
    }

    bool hauoli_enableDoppler(bool enabled) {
        return [[HauoliTrackerUnity sharedInstance] enableDoppler:enabled];
    }

    double hauoli_getVel(int spk_idx, int mic_idx) {
        return [[HauoliTrackerUnity sharedInstance] getVel:spk_idx mic_idx:mic_idx];
    }

    void hauoli_getCaliThrd(double *thrds) {
        return [[HauoliTrackerUnity sharedInstance] getCaliThrd:thrds];
    }
    
    // Gesture
    int hauoli_getGesture() {
        return [[HauoliTrackerUnity sharedInstance] getGesture];
    }

    int hauoli_getGestCnt() {
        return [[HauoliTrackerUnity sharedInstance] getGestCnt];
    }

    int hauoli_getGestDirChange() {
        return [[HauoliTrackerUnity sharedInstance] getGestDirChange];
    }

    bool hauoli_enableGesture(bool enabled) {
        return [[HauoliTrackerUnity sharedInstance] enableGesture:enabled];
    }

    // UI Related
    bool hauoli_trajCleaned() {
        return [[HauoliTrackerUnity sharedInstance] trajCleaned];
    }

    // AAudio
    void hauoli_setRecordingDeviceId(int id) {
        [[HauoliTrackerUnity sharedInstance] setRecordingDeviceId:id];
    }

    void hauoli_setPlaybackDeviceId(int id) {
        [[HauoliTrackerUnity sharedInstance] setPlaybackDeviceId:id];
    }
}
