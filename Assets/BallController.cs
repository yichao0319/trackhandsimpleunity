﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;
using UnityEngine;


using System.IO;

public class BallController : MonoBehaviour
{

    // ****************************************
    // Android
    // ****************************************
#if UNITY_ANDROID && !UNITY_EDITOR
    AndroidJavaClass hauoliTracker = null;

    // ****************************************
    // iOS
    // ****************************************
#elif UNITY_IOS && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern int hauoli_initTracker(int num_spk, int num_mic, double[] init_pos, double[] spk_pos, double[] mic_pos);
    [DllImport("__Internal")]
    private static extern int hauoli_start();
    [DllImport("__Internal")]
    private static extern int hauoli_stop();
    [DllImport("__Internal")]
    private static extern double hauoli_getDist();
    [DllImport("__Internal")]
    private static extern double[] hauoli_getDistsHist(int spk_idx, int mic_idx);
    [DllImport("__Internal")]
    private static extern void hauoli_getDistsHist2(double[] dists, int spk_idx, int mic_idx);
    [DllImport("__Internal")]
    private static extern void hauoli_getDists(double[] dists);
    [DllImport("__Internal")]
    private static extern void hauoli_getPos(double[] pos);
    [DllImport("__Internal")]
    private static extern double hauoli_getPosX();
    [DllImport("__Internal")]
    private static extern double hauoli_getPosY();
    [DllImport("__Internal")]
    private static extern double hauoli_getPosZ();
    [DllImport("__Internal")]
    private static extern double hauoli_getState();
    [DllImport("__Internal")]
    private static extern double hauoli_getPower();
    [DllImport("__Internal")]
    private static extern double hauoli_getB();
    [DllImport("__Internal")]
    private static extern void hauoli_getFc(int[] f);
    [DllImport("__Internal")]
    private static extern bool hauoli_getInternalData(double[] data, int len);
    // [DllImport("__Internal")]
    // private static extern void hauoli_getPlayBuffer(short int[] buf, int spk_idx);
    [DllImport("__Internal")]
    private static extern bool hauoli_setDistEstMethod(int method);
    [DllImport("__Internal")]
    private static extern bool hauoli_setB(double b);
    [DllImport("__Internal")]
    private static extern bool hauoli_setFc(int[] f);
    [DllImport("__Internal")]
    private static extern bool hauoli_setSkipTime(double time);
    [DllImport("__Internal")]
    private static extern bool hauoli_setUseFile(bool if_file);
    [DllImport("__Internal")]
    private static extern bool hauoli_setRecordAudio(bool if_record);
    [DllImport("__Internal")]
    private static extern bool hauoli_setSaveDist(bool if_save);
    [DllImport("__Internal")]
    private static extern bool hauoli_setAudioSource(int src);
    [DllImport("__Internal")]
    private static extern bool hauoli_setResetMethod(int method);
    // [DllImport("__Internal")]
    // private static extern bool hauoli_pushAudio(int8_t[] y, int num_elem);
    // [DllImport("__Internal")]
    // private static extern bool hauoli_pushAudio2(short int[] y, int num_elem);
    // Tap
    [DllImport("__Internal")]
    private static extern int hauoli_getNSeq();
    [DllImport("__Internal")]
    private static extern int hauoli_getNSeqUp();
    [DllImport("__Internal")]
    private static extern int hauoli_getMinTap();
    [DllImport("__Internal")]
    private static extern int hauoli_getMaxTap();
    [DllImport("__Internal")]
    private static extern int hauoli_getTapStep();
    [DllImport("__Internal")]
    private static extern void getCirDiff(double[] cir_dif_real, double[] cir_dif_imag);
    [DllImport("__Internal")]
    private static extern bool hauoli_setSeq(int num_seq, int num_seq_up);
    // [DllImport("__Internal")]
    // private static extern bool hauoli_setNSeq(int num_seq);
    // [DllImport("__Internal")]
    // private static extern bool hauoli_setNSeqUp(int num_seq_up);
    [DllImport("__Internal")]
    private static extern bool hauoli_setMinTap(int min_tap);
    [DllImport("__Internal")]
    private static extern bool hauoli_setMaxTap(int max_tap);
    [DllImport("__Internal")]
    private static extern bool hauoli_setTapStep(int tap_step);
    [DllImport("__Internal")]
    private static extern bool hauoli_setAutoThrd(int type, double thrd);
    [DllImport("__Internal")]
    private static extern bool hauoli_setAutoThrd2(int type, double[] thrds);
    [DllImport("__Internal")]
    private static extern bool hauoli_needCalibration();
    [DllImport("__Internal")]
    private static extern bool hauoli_setAbsDistMagThrd(double thrd);
    [DllImport("__Internal")]
    private static extern bool hauoli_setAbsDistDifThrd(double thrd);
    [DllImport("__Internal")]
    private static extern bool hauoli_setAbsDistItvThrd(double thrd);
    [DllImport("__Internal")]
    private static extern bool hauoli_setTapSelection(int method);
    [DllImport("__Internal")]
    private static extern bool hauoli_enableDoppler(bool enabled);
    [DllImport("__Internal")]
    private static extern double hauoli_getVel(int spk_idx, int mic_idx);
    [DllImport("__Internal")]
    private static extern void hauoli_getCaliThrd(double[] thrds);
    // Gesture
    [DllImport("__Internal")]
    private static extern int hauoli_getGesture();
    [DllImport("__Internal")]
    private static extern int hauoli_getGestCnt();
    [DllImport("__Internal")]
    private static extern int hauoli_getGestDirChange();
    [DllImport("__Internal")]
    private static extern bool hauoli_enableGesture(bool enabled);
    // UI Related
    [DllImport("__Internal")]
    private static extern bool hauoli_trajCleaned();
    // AAudio
    [DllImport("__Internal")]
    private static extern void hauoli_setRecordingDeviceId(int id);
    [DllImport("__Internal")]
    private static extern void hauoli_setPlaybackDeviceId(int id);
#endif

    private int nDim                             = 2;
    private int nSpk                             = 1;
    private int nMic                             = 1;
    private double[] initPos;
    private double[] spkPos;
    private double[] micPos;
    private bool ready                           = false;
    private double prevDist                      = 100;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("[Hauoli] Start Start()");
        //-------------------------------
        // Initialization
        initPos = new double[nDim];
        initPos[0] = 0;
        initPos[1] = -100;
        spkPos  = new double[nDim * nSpk];
        spkPos[0] = 0;
        spkPos[1] = 0;
        micPos  = new double[nMic * nDim];
        micPos[0] = 0;
        micPos[1] = 0;
        
#if UNITY_ANDROID && !UNITY_EDITOR
        if(hauoliTracker == null) {
            hauoliTracker = new AndroidJavaClass("com.hauoli.trackhand.HauoliTracker");
            Debug.Log("[Hauoli] " + hauoliTracker + ", spk" + nSpk + "=" + spkPos[0] + "," + spkPos[1] + ", mic" + nMic + "=" + micPos[0] + "," + micPos[1] + ", init=" + initPos[0] + "," + initPos[1]);
            int reti = hauoliTracker.CallStatic<int>("initTracker", nSpk, nMic, initPos, spkPos, micPos);
            bool retb = hauoliTracker.CallStatic<bool>("setDistEstMethod", 0);
            Debug.Log("[Hauoli] setDistEstMethod=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setAudioSource", 3);
            Debug.Log("[Hauoli] setAudioSource=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setSeq", 30, 240, 6000);
            Debug.Log("[Hauoli] setSeq=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setMinTap", 10);
            Debug.Log("[Hauoli] setMinTap=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setMaxTap", 250);
            Debug.Log("[Hauoli] setMaxTap=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setTapStep", 20);
            Debug.Log("[Hauoli] setTapStep=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setResetMethod", 0);
            Debug.Log("[Hauoli] setResetMethod=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setAutoThrd", 2, 0.0);
            Debug.Log("[Hauoli] setAutoThrd=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setRecordAudio", false);
            Debug.Log("[Hauoli] setRecordAudio=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setSaveDist", false);
            Debug.Log("[Hauoli] setSaveDist=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("setUseFile", false);
            Debug.Log("[Hauoli] setUseFile=" + retb);
            retb &= hauoliTracker.CallStatic<bool>("enableGesture", false);
            Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", conf=" + retb);

            bool recali = hauoliTracker.CallStatic<bool>("needCalibration");
            Debug.Log("[Hauoli] needCalibration=" + recali);            

            reti = hauoliTracker.CallStatic<int>("start");
            Debug.Log("[Hauoli] start=" + reti);
            if(reti == 1) { ready = true; }
        }
#elif UNITY_IOS && !UNITY_EDITOR
        Application.RequestUserAuthorization(UserAuthorization.Microphone);
        if (Application.HasUserAuthorization(UserAuthorization.Microphone)) {
            Debug.Log("Microphone found");
        }
        else {
            Debug.Log("Microphone not found");
        }

        int  reti = hauoli_initTracker(nSpk, nMic, initPos, spkPos, micPos);
        // bool retb = hauoli_setDistEstMethod(0);
        // retb &= hauoli_setAudioSource(4);
        // retb &= hauoli_setSeq(30, 240);
        // retb &= hauoli_setMinTap(10);
        // retb &= hauoli_setMaxTap(200);
        // retb &= hauoli_setTapStep(20);
        // retb &= hauoli_setResetMethod(0);
        // // retb &= hauoli_setAutoThrd(4, 0.00001);
        // retb &= hauoli_setAutoThrd(1, 0);
        // retb &= hauoli_setAbsDistMagThrd(0.00004);
        // retb &= hauoli_setAbsDistDifThrd(100);
        // retb &= hauoli_setAbsDistItvThrd(1);
        // retb &= hauoli_setTapSelection(0);
        // retb &= hauoli_setRecordAudio(false);
        // retb &= hauoli_setSaveDist(false);
        // retb &= hauoli_setUseFile(false);
        // retb &= hauoli_enableGesture(false);
        bool retb = true;
        retb &= hauoli_setDistEstMethod(0);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 1=" + retb);
        retb &= hauoli_setAudioSource(4);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 2=" + retb);
        retb &= hauoli_setSeq(30, 240);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 3=" + retb);
        retb &= hauoli_setMinTap(10);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 4=" + retb);
        retb &= hauoli_setMaxTap(200);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 5=" + retb);
        retb &= hauoli_setTapStep(20);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 6=" + retb);
        retb &= hauoli_setSkipTime(1.5);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 7=" + retb);
        retb &= hauoli_setResetMethod(0);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 8=" + retb);
        retb &= hauoli_setAutoThrd(1, 0);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 9=" + retb);
        retb &= hauoli_setAbsDistMagThrd(0.00004);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 10=" + retb);
        retb &= hauoli_setAbsDistDifThrd(100);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 11=" + retb);
        retb &= hauoli_setAbsDistItvThrd(1);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 12=" + retb);
        retb &= hauoli_setTapSelection(0);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 13=" + retb);
        retb &= hauoli_setRecordAudio(false);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 14=" + retb);
        retb &= hauoli_setSaveDist(false);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 15=" + retb);
        retb &= hauoli_setUseFile(false);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 16=" + retb);
        retb &= hauoli_enableGesture(false);
        Debug.Log("[Hauoli] HauoliTracker init=" + reti + ", 17=" + retb);

        reti = hauoli_start();
        Debug.Log("[Hauoli] start=" + reti);
        if(reti == 1) { ready = true; }
#endif
        Debug.Log("[Hauoli] End Start()");
    }

    // Update is called once per frame
    void Update()
    {
        double dist  = 100;
        if(ready) {
#if UNITY_ANDROID && !UNITY_EDITOR
            dist = hauoliTracker.CallStatic<double>("getDist");
#elif UNITY_IOS && !UNITY_EDITOR
            dist = hauoli_getDist();
            // hauoli_getDistsHist();
#endif
            // if(Math.Abs(dist - prevDist) > 5) {
            //     prevDist = dist;
            //     Debug.Log("[Hauoli] dist=" + dist);
            // }
            Debug.Log("[Hauoli] dist=" + dist);
        }
    }
}
