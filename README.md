# Andriod

* Required Files (copy them to PROJ/Assets/HauoliTracker/):

```
HauoliTrackHand.aar (Hauoli SDK)
openCVLibrary300-release.aar
```

# iOS

* Required Files (copy them to PROJ/Assets/HauoliTracker/):

```
HauoliConst.h (Hauoli SDK)
HauoliTracker.h (Hauoli SDK)
libHauoliTrack.a (Hauoli SDK)
HauoliTrackerUnity.h
HauoliTrackerUnity.mm
opencv2.framework
```

* Note

  * in "Project" view, select "Assets/HauoliTracker/libHauoliTrack.a" and check "Inspector" window. Make sure that only "iOS" is selected.

  * After generating and opening XCode project, 
  
    1. Update the signing information.

    2. Disable bitcode.

    3. In info.plist, add “Privacy - Microphone Usage Description”
