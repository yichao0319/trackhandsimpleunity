//
//  HauoliTrackerUnity.h
//  trackhand
//
//  Created by Lin Guan on 2/8/19.
//  Copyright © 2019 Hauoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HauoliTracker.h"

#ifndef HauoliTrackerUnity_h
#define HauoliTrackerUnity_h

@interface HauoliTrackerUnity : NSObject
{
    HauoliTracker *hauoliTracker;
}

+(HauoliTrackerUnity*) sharedInstance;
- (id) init;
- (int)initTracker:(int)num_spk
           num_mic:(int)num_mic
          init_pos:(double *)init_pos
           spk_pos:(double *)spk_pos
           mic_pos:(double *)mic_pos;
- (int)start;
- (int)stop;
- (double)getDist;
- (double *)getDistsHist:(int)spk_idx mic_idx:(int)mic_idx;
- (void)getDistsHist2:(double *)dists spk_idx:(int)spk_idx mic_idx:(int)mic_idx;
- (void)getDists:(double *)dists;
- (void)getPos:(double *)pos;
- (double)getPosX;
- (double)getPosY;
- (double)getPosZ;
- (double)getPower;
- (int)getState;
// General Config
- (double)getB;
- (void)getFc:(int *)f;
- (bool)getInternalData:(double *)data len:(int)len;
- (void)getPlayBuffer:(short int *)buf spk_idx:(int)spk_idx;
- (bool)setDistEstMethod:(int)method;
- (bool)setB:(double)b;
- (bool)setFc:(int *)f;
- (bool)setSkipTime:(double)time;
- (bool)setUseFile:(bool)if_file;
- (bool)setRecordAudio:(bool)if_record;
- (bool)setSaveDist:(bool)if_save;
- (bool)setAudioSource:(int)src;
- (bool)setResetMethod:(int)method;
- (bool)pushAudio:(int8_t *)y num_elem:(int)num_elem;
- (bool)pushAudio2:(int16_t *)y num_elem:(int)num_elem;
// Tap
- (int)getNSeq;
- (int)getNSeqUp;
- (int)getMinTap;
- (int)getMaxTap;
- (int)getTapStep;
- (void)getCirDiff:(double *)cir_dif_real cir_dif_imag:(double *)cir_dif_imag;
- (bool)setSeq:(int)num_seq num_seq_up:(int)num_seq_up;
// - (bool)setNSeq:(int)num_seq;
// - (bool)setNSeqUp:(int)num_seq_up;
- (bool)setMinTap:(int)min_tap;
- (bool)setMaxTap:(int)max_tap;
- (bool)setTapStep:(int)tap_step;
- (bool)setAutoThrd:(int)type thrd:(double)thrd;
- (bool)setAutoThrd2:(int)type thrds:(double *)thrds;
- (bool)needCalibration;
- (bool)setAbsDistMagThrd:(double)thrd;
- (bool)setAbsDistDifThrd:(double)thrd;
- (bool)setAbsDistItvThrd:(double)thrd;
- (bool)setTapSelection:(int)method;
- (bool)enableDoppler:(bool)enabled;
- (double)getVel:(int)spk_idx mic_idx:(int)mic_idx;
- (void)getCaliThrd:(double *)thrds;
// FMCW-sepcific
- (double)getChirpLenS;
- (bool)setChirpLenS:(double)chirp_len_s seq_len_s:(double)seq_len_s;
- (bool)setCancelLenS:(double)len;
- (bool)setAGC:(bool)on;
- (bool)setPlayAudio:(bool)play;
- (bool)setIfProcessAudio:(bool)process;
// Gesture
- (int)getGesture;
- (int)getGestCnt;
- (int)getGestDirChange;
- (bool)enableGesture:(bool)enabled;
// UI Related
- (bool)trajCleaned;
// AAudio
- (void)setRecordingDeviceId:(int)id;
- (void)setPlaybackDeviceId:(int)id;

@end

#endif /* HauoliTrackerUnity_h */
